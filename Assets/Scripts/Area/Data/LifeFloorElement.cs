﻿
namespace BlackCaviarGame.TestDiggerGame
{
    public class LifeFloorElement : FloorElement
    {
        public new LifeData data;

        public LifeFloorElement(LifeFloorElement floorElement) : base(floorElement)
        {

            data = floorElement.data.Clone() as LifeData;
        }
        public override object Clone()
        {
            return new LifeFloorElement(this);
        }
        public class LifeData : Data
        {
            public bool isAlive;
            public LifeData(LifeData data) : base(data)
            {
                isAlive = data.isAlive;
            }
            public override object Clone()
            {
                return new LifeData(this);
            }
            

         
        }
    }
}
