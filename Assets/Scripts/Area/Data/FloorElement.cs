using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{

    public class FloorElement:ICloneable
    {
        public FloorElementType type;
        public Data data;
        public void Initialize()
        {

        }

        public virtual object Clone()
        {
            return new FloorElement(this) as FloorElement;
        }

        public class Data : ICloneable
        {
            public Data()
            {
            }

            public Data(Data data)
            {
               
            }

            public virtual object Clone()
            {
                return new Data(this);
            }

           
        }
        public FloorElement(FloorElement floorElement)
        {
            data = floorElement.data?.Clone() as Data ?? new Data();
            type = floorElement.type;
        }
    }
}
