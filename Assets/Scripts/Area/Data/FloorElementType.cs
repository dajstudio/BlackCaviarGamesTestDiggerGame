﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "FloorElementType", menuName = "Game/Configuration/FloorElementType")]
    public class FloorElementType: ScriptableObject,ISerializeReferenceByAssetGuid
    {

    }
}