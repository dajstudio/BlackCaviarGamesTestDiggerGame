using Sirenix.OdinInspector;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "AreaGeneratorDifficult_N", menuName = "Game/Configuration/AreaGenerator")]
    public class AreaGenerator: ScriptableObject
    {
        [Title("For generate")]
        [LabelText("Field"),SerializeField]
        public FieldSettings possibleField;
        [Space]
        [LabelText("possibleCells")]
        public List<CellSettings> possibleCells;
        [Space]
        [LabelText("possibleFloors")]
        public List<FloorSettings> possibleFloors;

        public string savingLoadingKeyName => System.Guid.NewGuid().ToString();
        
        public Area Generate()
        {
            Area area = new Area();
            area.field = new Field();
            area.field = GenerateField(possibleField);
            area.cells = new List<Cell>();
            area.cells = GenerateCells(area.field, possibleCells);

            area.floors = new List<Floor>();
            area.floors = GenerateFloors(area.cells, possibleFloors);
            return area;
        }
        private Field GenerateField(FieldSettings possibleField)
        {
            Field field = new Field(possibleField.data);
            return field;
        }
        private List<Cell> GenerateCells(Field field, List<CellSettings> possibleCells) 
        {
            List<Cell> cells = new List<Cell>();
            int indexCell;
            for (int i = 0; i < field.columnAmount * field.rowAmount; i++)
            {
                indexCell = Random.Range(0, possibleCells.Count);
                Cell cell = new Cell(possibleCells[indexCell].data);
                cells.Add(cell);
            }
            return cells;
        }
        private List<Floor> GenerateFloors(List<Cell> cells, List<FloorSettings> possibleFloors)
        {
            List<Floor> floors = new List<Floor>();
            int indexPossibleFloor;
            for (int i = 0; i < cells.Count; i++)
            {
                for (int j = 0; j < cells[i].depth; j++)
                {
                    int _i = i;
                    int _j = j;
                    int[] _index = new int[2] { _i, _j };

                    indexPossibleFloor = Random.Range(0, possibleFloors.Count);
                    Floor floor = new Floor(possibleFloors[indexPossibleFloor].data);
                   
                    floor.color = new Color(
                            floor.color.r + (float)_index[1] / 10,
                            floor.color.g + (float)_index[1] / 10,
                            floor.color.b + (float)_index[1] / 10
                            );

                    floor.Initialize(_index);
                    floors.Add(floor);
                }
            }
            return floors;
        }


    }
}
