using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame 
{
 [Serializable]
    public class Cell
    {
        [HideInInspector]
        public int index = -1;
        [HideInInspector]
        public List<Floor> floors = new List<Floor>();
        public int depth;
        public Color color;
        public void Initialize(int index)
        {
            this.index = index;
        }
        public Cell(Cell cell)
        {
            this.index = cell.index;
            this.depth = cell.depth;
            floors = new List<Floor>();
            floors.AddRange(cell.floors);
            color = new Color();
            color = cell.color;
        }
    }
}