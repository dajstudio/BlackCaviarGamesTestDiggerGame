
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{

    public  class Floor 
    {
        [ShowInInspector]
        public int[] index = new int[2];
        [ShowInInspector,NonSerialized,OdinSerialize]
        public List<FloorElement> elements;
        public Color color;
        public void Initialize(int[] index)
        {
            this.index = index;
        }
        public Floor(Floor floor)
        {
            elements = new List<FloorElement>();
            for (int i = 0; i < floor.elements.Count; i++)
            {
                elements.Add(floor.elements[i].Clone() as FloorElement);
            }
            color = new Color(floor.color.r, floor.color.g, floor.color.b, floor.color.a);
        }
       
    }
    
}
