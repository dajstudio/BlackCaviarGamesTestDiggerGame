using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "typeFieldSettings", menuName = "Game/Configuration/Field")]
    public class FieldSettings : SerializedScriptableObject
    {
        [ShowInInspector]
        public Field data;
    }
}
