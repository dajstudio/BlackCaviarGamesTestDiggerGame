
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    
    public class Field 
    {
        public string technicalName;
        public int rowAmount;
        public int columnAmount;
        public Color colorMainBackground;
        public Color colorContainerBackground;

        public Field()
        {

        }

        public Field(Field field)
        {
            technicalName = field.technicalName;
            rowAmount = field.rowAmount;
            columnAmount = field.columnAmount;
            colorContainerBackground = field.colorContainerBackground;
            colorMainBackground = field.colorMainBackground;
        }
    }
}