using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "typeFloorSettings", menuName = "Game/Configuration/Floor")]
    public class FloorSettings : SerializedScriptableObject
    {
        [ShowInInspector]
        public Floor data;
    }
}
