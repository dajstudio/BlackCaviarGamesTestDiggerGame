using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
   
    public class HaveLootFloorElement :  FloorElement
    {

        public new LootData data;

        public HaveLootFloorElement(HaveLootFloorElement floorElement) : base(floorElement)
        {
            data = floorElement.data.Clone() as LootData;
        }
        public override object Clone()
        {
            return new HaveLootFloorElement(this);
        }

        public class LootData : Data
        {
            public ILooting loot;

            public LootData(LootData data) : base(data)
            {
                loot = data.loot;
            }

            public override object Clone()
            {
                return new LootData(this);
            }
        }
      

      
    }
}
