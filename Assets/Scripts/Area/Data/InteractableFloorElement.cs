using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
    public class InteractableFloorElement :   FloorElement
    {
        public new InteractableData data;

        public InteractableFloorElement(InteractableFloorElement floorElement) : base(floorElement)
        {
            data = floorElement.data.Clone() as InteractableData;
        }
        public override object Clone()
        {
            return new InteractableFloorElement(this);
        }

        public class InteractableData : Data
        {
            public bool isInteracted;
            public InteractableData(InteractableData data) : base(data)
            {
                isInteracted = data.isInteracted;
            }

            public override object Clone()
            {
                return new InteractableData(this);
            }
        }
     
    }
}
