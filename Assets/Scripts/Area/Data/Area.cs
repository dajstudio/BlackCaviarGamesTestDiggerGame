using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections.Generic;

namespace BlackCaviarGame.TestDiggerGame
{

    public class Area
    {
        [ShowInInspector, NonSerialized, OdinSerialize]
        public Field field = new Field();
        [ShowInInspector, NonSerialized, OdinSerialize]
        public List<Cell> cells = new List<Cell>();
        [ShowInInspector, NonSerialized, OdinSerialize]
        public List<Floor> floors = new List<Floor>();
    }
}
