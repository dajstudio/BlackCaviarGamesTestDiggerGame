using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "typeCellSettings", menuName = "Game/Configuration/Cell")]
    public class CellSettings : SerializedScriptableObject
    {
        [ShowInInspector,NonSerialized,OdinSerialize]
        public Cell data;
    }
}
