using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class FloorElementViewer : MonoBehaviour
    {

        public FloorElementType floorElementType;
        private FloorElement _floorElement;
        public virtual FloorElementViewer Instatiate(GameObject destinationGameObject, FloorElement floorElement)
        {
            _floorElement = floorElement;
            FloorElementViewer floorElementViewer = destinationGameObject.AddComponent(this);
            floorElementViewer.Initialize();
            return this;
        }
        public virtual void Initialize()
        {

        }
    }
}
