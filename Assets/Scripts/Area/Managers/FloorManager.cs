using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class FloorManager : MonoBehaviour
    {
        [ShowInInspector]
        protected Floor _floor;

        public virtual void Initialize(Floor floor,int[] index)
        {

            _floor = floor;
            _floor.Initialize(index);

            foreach (FloorElement element in this._floor.elements)
            {
                element.Initialize();
            }
        }


    }
}