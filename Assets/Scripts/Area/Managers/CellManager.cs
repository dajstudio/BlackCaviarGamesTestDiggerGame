using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class CellManager : MonoBehaviour
    {
        public Cell cell;
        public Transform container;
        public virtual void Initialize(Cell cell, int index)
        {
            cell.Initialize(index);
            this.cell = cell;
        }
    }
}