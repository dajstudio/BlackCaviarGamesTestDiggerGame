using Sirenix.OdinInspector;
using UnityEngine;
using System;
using Sirenix.Serialization;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class FieldManager : SerializedMonoBehaviour
    {
        [ShowInInspector]
        public Field field;
        public Transform container;

        [Button,ShowIf("field")]
        public void Initialize()
        {
            Initialize(field);
        }
        public virtual void Initialize(Field field)
        {
            this.field = field;
            
        }
     
    }
}
