using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackCaviarGame.TestDiggerGame
{
    [RequireComponent(typeof(Image))]
    public class CanvasFloorManager : FloorManager
{
        public Image backgroundImage;
        private RectTransform _rectTransform;
        [ShowInInspector, AssetsOnly]
        public List<FloorElementController> prefabFloorElementControllers;
        [ShowInInspector, AssetsOnly]
        public List<FloorElementViewer> prefabFloorElementViewers;
        public override void Initialize(Floor floor, int[] index)
        {
            gameObject.name += $"_{index[0]}_{index[1]}";
            base.Initialize(floor, index);
            foreach (var floorElement in _floor.elements)
            {
                CreateFloorElement(floorElement);
            }

            backgroundImage.color = _floor.color;
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -index[1]);
            _rectTransform = GetComponent<RectTransform>();

        }
        public void CreateFloorElement(FloorElement floorElement)
        {
            foreach (var prefabFloorElementController in prefabFloorElementControllers)
            {
                if(prefabFloorElementController.floorElementType == floorElement.type)
                {
                   FloorElementController floorElementController = prefabFloorElementController.Instatiate(gameObject,floorElement);
                    floorElementController.Initialize();
                }
            }
            foreach (var prefabFloorElementViewer in prefabFloorElementViewers)
            {
                if (prefabFloorElementViewer.floorElementType == floorElement.type)
                {
                   FloorElementViewer floorElementViewer = prefabFloorElementViewer.Instatiate(gameObject, floorElement);
                    floorElementViewer.Initialize();
                }
            }
        }
    } 
}
