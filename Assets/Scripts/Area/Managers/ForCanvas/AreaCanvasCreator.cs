

using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    //TODO: rename to CanvasAreaManagerCreator
    [CreateAssetMenu(fileName = "AreaCanvasCreator", menuName = "Game/Creators/AreaCanvasCreator")]
    public class AreaCanvasCreator : AreaManagerFromPrefabsCreator
    {


        internal override FieldManager CreateFieldManager(Field Field)
        {
            CanvasFieldManager fieldViewer = Instantiate(prefabFieldManager, _areaManager.transform).GetComponent<CanvasFieldManager>();
            fieldViewer.Initialize(Field);

            return fieldViewer;
        }

        internal override List<CellManager> CreateCellManagers(FieldManager fieldManager, List<Cell> CellsData)
        {
            List<CellManager> cellManagers = new List<CellManager>();
            int indexCell = new int();
            for (int i = 0; i < fieldManager.field.columnAmount * fieldManager.field.rowAmount; i++)
            {

                CanvasCellManager cellManager = Instantiate(prefabCellManager, fieldManager.container).GetComponent<CanvasCellManager>();
                cellManager.Initialize(CellsData[indexCell], i);
                cellManagers.Add(cellManager);
            }
            return cellManagers;
        }

        internal override List<FloorManager> CreateFloorManagers(List<CellManager> cellManagers, List<Floor> floorsData)
        {
            List<FloorManager> floorManagers = new List<FloorManager>();
            int indexFloor = new int();
            for (int i = 0; i < cellManagers.Count; i++)
            {
                for (int j = 0; j < cellManagers[i].cell.depth; j++)
                {


                    indexFloor = floorsData.FindIndex(x => x.index[0] == i && x.index[1] == j);

                    CanvasFloorManager floorManager = Instantiate(prefabFloorManager, cellManagers[i].container).GetComponent<CanvasFloorManager>();
                    floorManager.Initialize(floorsData[indexFloor], new int[] { i, j });
                    floorManagers.Add(floorManager);
                }
            }
            return floorManagers;
        }


    }
}
