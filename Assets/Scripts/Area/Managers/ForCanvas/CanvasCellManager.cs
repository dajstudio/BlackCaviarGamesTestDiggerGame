using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackCaviarGame.TestDiggerGame
{
    public class CanvasCellManager : CellManager
    {
        public Image background;
        // Start is called before the first frame update
        public override void Initialize(Cell cell, int index)
        {
            base.Initialize(cell, index);
            background.color = cell.color;
        }
    }
}
