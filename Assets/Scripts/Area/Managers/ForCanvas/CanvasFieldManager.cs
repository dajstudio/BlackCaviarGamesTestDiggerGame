using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlackCaviarGame.TestDiggerGame
{
    public class CanvasFieldManager : FieldManager
    {
        [Required]
        public Image background;
        internal RectTransform containerRect;
        [Required]
        public Image containerBackground;
        [Required]
        public GridLayoutGroup containerGridLayoutGroup;

        public override void Initialize(Field field)
        {
            base.Initialize(field);
            this.field = field;

            containerRect = container.GetComponent<RectTransform>();

            background.color = this.field.colorMainBackground;
            containerBackground.color = this.field.colorContainerBackground;

            containerGridLayoutGroup.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
            containerGridLayoutGroup.constraintCount = field.columnAmount;
            containerGridLayoutGroup.cellSize = new Vector2(
              (containerRect.rect.width - (containerGridLayoutGroup.spacing.x * (field.columnAmount - 2)) - containerGridLayoutGroup.padding.left - containerGridLayoutGroup.padding.right) / field.columnAmount,
               (containerRect.rect.height - (containerGridLayoutGroup.spacing.y * (field.rowAmount - 2)) - containerGridLayoutGroup.padding.bottom - containerGridLayoutGroup.padding.top) / field.rowAmount
               );
        
        }
    }
}
