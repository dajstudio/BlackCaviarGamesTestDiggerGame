using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public class LifeFloorElementController : FloorElementController
    {
        private IHavingLoot _havingLoot;
        private LifeFloorElement _lifeFloorElement;
        public override void Initialize()
        {
            base.Initialize();
            //TODO: show in editor type and event for AddListener
            _havingLoot = gameObject.GetComponent<IHavingLoot>();
            _havingLoot?.OnLootRemoved.AddListener(Kill);
            _havingLoot?.OnNegativeResponseGiveMeLoot.AddListener(Kill);
            _lifeFloorElement = (_floorElement as LifeFloorElement);
            if (_lifeFloorElement ==null)
            {
                Debug.LogError($"{name} LifeData is null");
            }

            ChangeStateOfLife(_lifeFloorElement.data.isAlive);
        }

        private void Kill()
        {
            ChangeStateOfLife(false);
        }
        private void ChangeStateOfLife(bool isAlive)
        {
            Debug.Log($"LifeFloorElementController.ChangeStateOfLife({isAlive})");
            gameObject.SetActive(isAlive);
            _lifeFloorElement.data.isAlive = isAlive;
        }
    }
}