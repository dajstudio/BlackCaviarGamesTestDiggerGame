using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class FloorElementController : MonoBehaviour
    { 
        public FloorElementType floorElementType;
        protected FloorElement _floorElement;
        public virtual FloorElementController Instatiate(GameObject destinationGameObject,FloorElement floorElement)
        {
            _floorElement = floorElement;
            FloorElementController floorElementController = destinationGameObject.AddComponent(this);
            return floorElementController;
        }
        public virtual void Initialize()
        {

        }
    }
}
