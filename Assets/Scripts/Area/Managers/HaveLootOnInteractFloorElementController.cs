using UnityEngine;
using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
    public class HaveLootOnInteractFloorElementController : FloorElementController, IHavingLoot
    {
        public bool isHaveLoot => loot != null;

        public ILooting loot
        { 
            get => _haveLootFloorElement.data.loot;
            set {
                _haveLootFloorElement.data.loot = value;
            } 
        }
        public UnityEvent OnLootAdded { get; set; }
        public UnityEvent OnLootRemoved { get; set; }
        public UnityEvent OnNegativeResponseGiveMeLoot { get ; set ; }

        private IInteractable _interactableElement;
        private HaveLootFloorElement _haveLootFloorElement;

        public void ProcessResponseGiveMeLoot(ILooting loot)
        {
            if(loot != null)
            {
                AddLoot(loot);
            }
            else
            {
                OnNegativeResponseGiveMeLoot?.Invoke();
            }
        }
        public void AddLoot(ILooting loot)
        {
            if (isHaveLoot || loot == null) 
                return;

            this.loot = loot;
            _interactableElement.CanInteract = false;

            OnLootAdded?.Invoke();
        }

        public override void Initialize()
        {
            base.Initialize();
            //TODO: show in editor type and event for AddListener
            _interactableElement = GetComponent<IInteractable>();
            _interactableElement?.OnInteracted.AddListener(RequestGiveMeLootData);

            OnLootAdded = new UnityEvent();
            OnLootRemoved = new UnityEvent();
            OnNegativeResponseGiveMeLoot = new UnityEvent();
            OnLootAdded.AddListener(RequestLootManager);

            _haveLootFloorElement = (_floorElement as HaveLootFloorElement);
            if (_haveLootFloorElement.data.loot != null)
            {
                RequestLootManager();
            }
           
        }

        public void RemoveLoot()
        {
            _haveLootFloorElement.data.loot = null;
            _interactableElement.CanInteract = true;

            OnLootRemoved?.Invoke();
        }
        public virtual void RequestLootManager()
        {
            EventManager.TriggerEvent(LootsCreator.RequestLootManagerEventName, gameObject);
        }
        public void RequestGiveMeLootData()
        {
            //TODO: Change to delegate
            EventManager.TriggerEvent(LootMachine.RequestGiveMeLootEventName, gameObject);
        }
        private void OnDestroy()
        {
            _interactableElement?.OnInteracted.RemoveListener(RequestGiveMeLootData);
        }
    }
}
