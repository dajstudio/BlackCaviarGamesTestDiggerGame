using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackCaviarGame.TestDiggerGame
{
    // TODO: Move Image and BoxCollider to viewModel and add instatiate additional component from its
    [RequireComponent(typeof(Image))]
    public class InteractableFloorElementController : FloorElementController, IInteractable
    {
        [ShowInInspector]
        public bool CanInteract { get; set; } = true;
        [ShowInInspector]
        public UnityEvent OnInteracted { get; set; }

        private InteractableFloorElement _interactableFloorElement;
        public override void Initialize()
        {
            base.Initialize();

            OnInteracted = new UnityEvent();
          
           
            _interactableFloorElement = (_floorElement as InteractableFloorElement);
            if (_interactableFloorElement == null)
            {
                Debug.LogError($"{name} InteractableFloorElement is null");
            }

            StartCoroutine(AddCollider());
        }
        public IEnumerator AddCollider()
        {
            yield return null;

            Vector2 size = GetComponent<RectTransform>().rect.size;

            BoxCollider2D boxCollider2D = gameObject.AddComponent<BoxCollider2D>();
            boxCollider2D.size = size;

        }
        public bool TryInteract()
        {
            if (CanInteract)
            {
                OnInteracted?.Invoke();
                return true;
            }
            return false;
        }



    }
}
