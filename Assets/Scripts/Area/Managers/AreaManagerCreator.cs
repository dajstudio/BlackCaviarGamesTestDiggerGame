using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public  abstract class AreaManagerCreator : ScriptableObject
    {
        [AssetsOnly]
        public AreaManager prefabAreaManager;
        [AssetsOnly]
        public FieldManager prefabFieldManager;
        [AssetsOnly]
        public CellManager prefabCellManager;
        [AssetsOnly]
        public FloorManager prefabFloorManager;


        protected AreaManager _areaManager;
        public virtual void Create(Area area)
        {
           
        }
    }
}
