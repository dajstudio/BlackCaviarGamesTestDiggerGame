using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public class AreaManager : MonoBehaviour
    {
        public Area area;

        [HideInInspector]
        public FieldManager FieldManager;
        [HideInInspector]
        public List<CellManager> cellManagers;
        [HideInInspector]
        public List<FloorManager> floorManagers;

        private void Start()
        {
            GetComponent<Canvas>().worldCamera = Camera.main;
        }
    }
}
