using System.Collections.Generic;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class AreaManagerFromPrefabsCreator : AreaManagerCreator
    {

        public override void Create(Area area)
        {
            _areaManager = Instantiate(prefabAreaManager);
            _areaManager.area = area;
            _areaManager.FieldManager = CreateFieldManager(area.field);
            _areaManager.cellManagers = CreateCellManagers(_areaManager.FieldManager, area.cells);
            _areaManager.floorManagers = CreateFloorManagers(_areaManager.cellManagers, area.floors);
        }

        internal abstract FieldManager CreateFieldManager(Field Field);
        internal abstract List<CellManager> CreateCellManagers(FieldManager fieldManager, List<Cell> Cells);
        internal abstract List<FloorManager> CreateFloorManagers(List<CellManager> cellManager, List<Floor> floorDatas);




    }
}
