using System.Collections.Generic;

namespace BlackCaviarGame.TestDiggerGame
{
    public interface ISavingLoadingItem
    {
        public string savingLoadingKeyName { get; }
        void Save();
        void Load();
        
      
    }
}
