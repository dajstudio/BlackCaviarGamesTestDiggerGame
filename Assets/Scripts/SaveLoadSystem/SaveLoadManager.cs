using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.Utilities;
using Sirenix.Serialization;
using System.IO;
using Sirenix.OdinInspector;

namespace BlackCaviarGame.TestDiggerGame
{
    public class SaveLoadManager : MonoBehaviour
    {
        public const string SaveEventName = "SaveEvent";
        public const string LoadEventName = "LoadEvent";

        public const string prefixKey = "YkmPks8)i.s_";
        public const string isHaveDataKeyName = "isHaveData";

        public const string extensionFile = ".amendtx";
        public const string nameFile = "saveData";

        public bool isHaveData => PlayerPrefs.HasKey(prefixKey + isHaveDataKeyName);

        public ScriptableObjectReferenceCache scriptableObjectReferenceCache;

        [ShowInInspector]
        private Dictionary<string, byte[]> _data = new Dictionary<string, byte[]>();

        private static SaveLoadManager _saveloadManager;

        public static SaveLoadManager instance
        {
            get
            {
                if (!_saveloadManager)
                {
                    _saveloadManager = FindObjectOfType(typeof(SaveLoadManager)) as SaveLoadManager;

                    if (!_saveloadManager)
                    {
                        Debug.LogError("There needs to be one active SaveLoadManager script on a GameObject in your scene.");
                    }
                    else
                    {
                        _saveloadManager.Init();
                    }
                }

                return _saveloadManager;
            }
        }
        void Init()
        {
            Application.wantsToQuit -= OnWantsToQuit;
            Application.wantsToQuit += OnWantsToQuit;
            Application.focusChanged -= OnWantsQuit;
            Application.focusChanged += OnWantsQuit;
            RemoveListeners();
            AddListeners();
        }
        public void AddListeners()
        {
           
        }
        public void RemoveListeners()
        {

        
        }
        public void DeleteSave()
        {
            PlayerPrefs.DeleteKey(prefixKey + isHaveDataKeyName);
            PlayerPrefs.Save();
            Debug.Log($"SaveLoadManager.DeleteSave()");
            _data = new Dictionary<string, byte[]>();
            BayatGames.SaveGameFree.SaveGame.DeleteAll();
        }
        private void OnDisable()
        {
            RemoveListeners();
            Application.focusChanged -= OnWantsQuit;
            Application.wantsToQuit -= OnWantsToQuit;
        }
        public void Touch()
        {

        }
        [Button]
        public void LoadData()
        {
            Debug.Log("SaveLoadMnager.LoadData()");
            _data = BayatGames.SaveGameFree.SaveGame.Load<Dictionary<string, byte[]>>(nameFile + extensionFile);
        }
        public void Save<T>(string id, T data) 
        {
            PlayerPrefs.SetInt(prefixKey + isHaveDataKeyName, 1);

            var context = new SerializationContext
            {
                StringReferenceResolver = scriptableObjectReferenceCache
            };

            if (this._data.ContainsKey(id))
            {
                this._data[id] = SerializationUtility.SerializeValue<T>(data, DataFormat.JSON,context);
            }
            else
            {
                this._data.Add(id, SerializationUtility.SerializeValue<T>(data, DataFormat.JSON,context));
            }
            Debug.Log($"{typeof(T).Name}.Save()");
        }
        public T Load<T>(string id) 
        {
            if (!_data.ContainsKey(id))
            {
                return default(T);
            }

            var context = new DeserializationContext
            {
                StringReferenceResolver = scriptableObjectReferenceCache
            };

            T value = SerializationUtility.DeserializeValue<T>(_data[id], DataFormat.JSON,context);
            Debug.Log(value);
            return value;

        }
        private void OnWantsQuit(bool focus)
        {
            if (focus || GameManager.isRestart)
                return;

            OnWantsToQuit();
        }
        private bool OnWantsToQuit()
        {
            EventManager.TriggerEvent(SaveEventName);

            WriteToDisk();

            return true;
        }
        [Button]
        private void WriteToDisk()
        {
            BayatGames.SaveGameFree.SaveGame.Save<Dictionary<string, byte[]>>(nameFile + extensionFile, _data);
        }

    }


}