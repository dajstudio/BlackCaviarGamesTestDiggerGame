﻿
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace BlackCaviarGame.TestDiggerGame
{
    public class EventManager : MonoBehaviour
    {
        //TODO: change to generic type;
        private Dictionary<string, UnityEvent> _eventDictionary;
        private Dictionary<string, UnityEvent<GameObject>> _eventGameObjectDictionary;
        private static EventManager _eventManager;

        public static EventManager instance
        {
            get
            {
                if (!_eventManager)
                {
                    _eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                    if (!_eventManager)
                    {
                        Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                    }
                    else
                    {
                        _eventManager.Init();
                    }
                }

                return _eventManager;
            }
        }

        void Init()
        {
            DontDestroyOnLoad(this);
            if (_eventDictionary == null)
            {
                _eventDictionary = new Dictionary<string, UnityEvent>();

            }
            if (_eventGameObjectDictionary == null)
            {
                _eventGameObjectDictionary = new Dictionary<string, UnityEvent<GameObject>>();

            }

        }
        public void Touch()
        {

        }
        public static void StartListening(string eventName, UnityAction listener)
        {
            UnityEvent thisEvent = null;
            if (instance._eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEvent();
                thisEvent.AddListener(listener);
                instance._eventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StopListening(string eventName, UnityAction listener)
        {
            if (_eventManager == null) return;
            UnityEvent thisEvent = null;
            if (instance._eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }
        public static void StartListening(string eventName, UnityAction<GameObject> listener)
        {
            UnityEvent<GameObject> thisEvent = null;
            if (instance._eventGameObjectDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEvent<GameObject>();
                thisEvent.AddListener(listener);
                instance._eventGameObjectDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StopListening(string eventName, UnityAction<GameObject> listener)
        {
            if (_eventManager == null) return;
            UnityEvent<GameObject> thisEvent = null;
            if (instance._eventGameObjectDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }

        public static void TriggerEvent(string eventName)
        {
            UnityEvent thisEvent = null;
            if (instance._eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.Invoke();
            }
           
        }
        public static void TriggerEvent(string eventName,GameObject gameObject)
        {
            UnityEvent<GameObject> thisGameObjectEvent = null;
            if (instance._eventGameObjectDictionary.TryGetValue(eventName, out thisGameObjectEvent))
            {
                thisGameObjectEvent.Invoke(gameObject);
            }

        }
       
}
}
