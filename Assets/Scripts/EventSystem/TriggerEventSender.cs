using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public class TriggerEventSender : MonoBehaviour
    {
        public void SendTriggerEvent(string eventName)
        {
            EventManager.TriggerEvent(eventName);
        }
    }
}
