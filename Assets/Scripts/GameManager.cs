using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public class GameManager : MonoBehaviour
    {
        public const string NewGameEventName = "NewGameEvent";
        public const string RestartEventName = "RestartEvent";

        public static bool isRestart = false;
        private void Start()
        {
            RemoveListeners();
            AddListeners();
            StartGame();
        }

        private static void StartGame()
        {
            Application.targetFrameRate = 60;
            EventManager.instance.Touch();
            SaveLoadManager.instance.Touch();

            if (!SaveLoadManager.instance.isHaveData)
            {
                StartNewGame();
            }
            else
            {
                SaveLoadManager.instance.LoadData();
                EventManager.TriggerEvent(SaveLoadManager.LoadEventName);
            }
        }

        private static void StartNewGame()
        {
            EventManager.TriggerEvent(NewGameEventName);
        }

        private void AddListeners()
        {
            EventManager.StartListening(RestartEventName, Restart);
        }
        private void RemoveListeners()
        {
            EventManager.StopListening(RestartEventName, Restart);

        }
        public void Restart()
        {
            SaveLoadManager.instance.DeleteSave();
            StartNewGame();
        }
        private void OnDisable()
        {
            RemoveListeners();
        }
    }
}