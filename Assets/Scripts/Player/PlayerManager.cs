using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class PlayerManager : SerializedMonoBehaviour
    {
        [ShowInInspector,NonSerialized,OdinSerialize]
        public Player player;

        public Transform container;
        public virtual void Initialize(Player player)
        {
            this.player = player;
            this.player.Initialize();
        }
        private void Update()
        {
            foreach (var playerWearble in player.playerWearables)
            {
                playerWearble.Update();
            }
        }
    }
}
