using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
    
    public interface IInteractable
    {
        public bool CanInteract { get; set; }
        public UnityEvent OnInteracted { get; set; }
        public bool TryInteract();
    }
}
