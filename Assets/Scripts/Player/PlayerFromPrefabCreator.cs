using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class PlayerFromPrefabCreator<T> : PlayerCreator
                                     where T : PlayerManager
    {
        [AssetsOnly]
        public T prefabPlayerManager;
        [NonSerialized, OdinSerialize]
        public List<IPlayerWearableViewer> prefabPlayerWearableViewers;

        protected T _playerManager;

        public override void Create(Player player)
        {
            _playerManager = Instantiate(prefabPlayerManager).GetComponent<T>();
            _playerManager.Initialize(player);
            foreach (var playerWearable in player.playerWearables)
            {
                playerWearable.Initialize();
                List<IPlayerWearableViewer> prefabForConcreteTypePlayerWearableViewers = prefabPlayerWearableViewers.FindAll(x => x.TypePlayerWearable == playerWearable);
                foreach (var prefabForConcreteTypePlayerWearableViewer in prefabForConcreteTypePlayerWearableViewers)
                {
                    MonoBehaviour playerWearableViewerMonobehaviour = prefabForConcreteTypePlayerWearableViewer as MonoBehaviour;
                    IPlayerWearableViewer playerWearableViewer = Instantiate(playerWearableViewerMonobehaviour, _playerManager.container, false).GetComponent<IPlayerWearableViewer>(); ;
                    playerWearableViewer.Initialize(_playerManager.gameObject);
                }
            }
        }
    }
}