using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackCaviarGame.TestDiggerGame
{
    //TODO: change functionality of different raycast from classes to scriptabelobject with return object:T by raycast
    [CreateAssetMenu(fileName = "PhysicRaycast2DPlayerWearableWeapon", menuName = "Game/Configuration/PlayerWearable/PhysicRaycast2DWeapon")]
    public class PhysicRaycast2DPlayerWearableWeapon : PlayerWearableWeapon
    {
        public override void Shot()
        {
            if (Input.GetMouseButtonUp(0) && Weapons.Count > 0)
            {

                base.Shot();
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y)),Vector2.zero,Mathf.Infinity);
                Debug.Log("Shot " + Input.mousePosition + " " + hit.point + " " + Camera.main.ScreenToWorldPoint(Input.mousePosition));
                if (hit.collider != null)
                {
                    IInteractable interactable = hit.collider.gameObject.GetComponent<IInteractable>();
                    Debug.Log("Interact");
                    if (interactable != null)
                    {
                        bool isInteracted = interactable.TryInteract();
                        if (isInteracted)
                        {
                            Debug.Log($"Weapon interact with {hit.collider.gameObject.name}");

                            interactable.OnInteracted?.Invoke();
                            DeleteWeapon();
                        }
                       
                        
                       
                    }
                }
            }
        }
    }
}
