using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "PlayerWearableBag", menuName = "Game/Configuration/PlayerWearable/Bag")]
    public class PlayerWearableBag : SerializedScriptableObject, IPlayerWearable
    {
        public Bag bag;
     

        public void Initialize()
        {
            
        }

        public void Update()
        {
            
        }
    }
}
