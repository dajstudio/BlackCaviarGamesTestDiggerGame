using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
    
    public abstract class PlayerWearableWeapon : SerializedScriptableObject, IPlayerWearable
    {
        public const string weaponsUpdateEventName = "weaponsUpdateEvent";

        private Stack<IWeapon> _weapons = new Stack<IWeapon>();
        [ShowInInspector]
        public Stack<IWeapon> Weapons
        {
            get { return _weapons; }
        }
        public UnityEvent<int> OnWeaponsCountChanged;

        [NonSerialized, OdinSerialize, ShowInInspector]
        private Bag _bag;

        public virtual void Initialize()
        {
            TakeAllWeapons();
            _bag.OnChangedConsumables.AddListener(TakeAllWeapons);
        }
        [Button]
        public void TakeAllWeapons()
        {
            List<Consumable> weaponConsumables =  _bag.Consumables.FindAll(x => x is IWeapon);
            if(
                weaponConsumables != null 
                && weaponConsumables.Count != _weapons.Count
                )
            {
                _weapons.Clear();
                foreach (var item in weaponConsumables)
                {
                    _weapons.Push(item as IWeapon);
                }
                OnWeaponsCountChanged?.Invoke(_weapons.Count);
            }
        }
        public void DeleteWeapon()
        {
           _bag.RemoveConsumable((Consumable) Weapons?.Peek()); 
        }


        public virtual void Update()
        {
            Shot();
        }
        public virtual void Shot()
        {
          

                    //Shot logic
        }
        public void OnDisable()
        {
            _bag.OnChangedConsumables.RemoveListener(TakeAllWeapons);
        }
    
    }
}