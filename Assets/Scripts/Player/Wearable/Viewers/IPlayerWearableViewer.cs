﻿using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public interface IPlayerWearableViewer
    {
        public IPlayerWearable TypePlayerWearable { get; set; }
        public abstract void Initialize(GameObject gameObject);
    }
}