using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public class PlayerLootingConsumableViewer : PlayerWearableViewer<PlayerWearableBag>
    {
        [ShowInInspector]
        public const string LootingInBagTextLocalizationKey = "LootingInBagDescriptionText";

        //TODO: Task#1 made for several types of looting consumables by add prefab with 2 text of element of vertical group in scroll view
        public TextMeshProUGUI DescriptionText;
        public TextMeshProUGUI ValueText;
        protected override void Awake()
        {
            base.Awake();
            ChangeContent();
            playerWearable.bag.OnChangedConsumables.AddListener(ChangeContent);
        }
        private void ChangeContent()
        {
            List<Consumable> lootingConsumables = playerWearable.bag.Consumables.FindAll(x => x is LootingConsumable);
                 DescriptionText.text = "No loot";
                ValueText.text =  "";

            if(lootingConsumables.Count > 0)
            {
                //TODO: make cut by textmeshpro ellipsis
                DescriptionText.text = $"{lootingConsumables[0].name.Substring(0,8)}:";
            }
            ValueText.text = lootingConsumables.Count.ToString();
            
        }
        private void OnDisable()
        {
            playerWearable.bag.OnChangedConsumables.RemoveListener(ChangeContent);
        }
    }
}
