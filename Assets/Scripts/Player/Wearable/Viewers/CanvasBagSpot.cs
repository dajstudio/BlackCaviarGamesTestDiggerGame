using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [RequireComponent(typeof(Collider2D))]
    public class CanvasBagSpot : PlayerWearableViewer<PlayerWearableBag>, IReceiverLootManager
    {
        public void AddLootManager(LootManager lootManager)
        {
           playerWearable.bag.AddConsumable((Consumable)lootManager.loot);
           Destroy(lootManager);
        }

    }
}