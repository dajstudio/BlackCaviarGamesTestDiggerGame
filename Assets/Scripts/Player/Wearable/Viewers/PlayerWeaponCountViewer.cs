using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace BlackCaviarGame.TestDiggerGame
{
    public class PlayerWeaponCountViewer : PlayerWearableViewer<PlayerWearableWeapon>
    {
        [ShowInInspector]
        public const string WeaponTextLocalizationKey = "WeaponDescriptionText";

        public TextMeshProUGUI DescriptionText;
        public TextMeshProUGUI ValueText;

        //TODO: for future animation
        private int _lastCount = new int();
        protected override void Awake()
        {
            base.Awake();
            ValueText.text = playerWearable.Weapons.Count.ToString();
            Debug.Log("wEAPONcOUNTvIEWER Awake");
            playerWearable.OnWeaponsCountChanged.AddListener(ChangeValue);
        }
        private void ChangeValue(int count)
        {
            Debug.Log($"CanvasWeaponViewer Weapon Count - {count}");
            ShowDifference(_lastCount - count);
            ValueText.text = count.ToString();
        }

        //TODO: for future animation
        private void ShowDifference(int diff)
        {

        }
        private void OnDisable()
        {
            playerWearable.OnWeaponsCountChanged.RemoveListener(ChangeValue);
        }
    }
}
