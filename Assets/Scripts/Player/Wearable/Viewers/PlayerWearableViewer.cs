using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{

    public abstract class PlayerWearableViewer<T> : SerializedMonoBehaviour, IPlayerWearableViewer
                                        where T:IPlayerWearable
    {
        
        protected T playerWearable;

        [ShowInInspector,OdinSerialize]
        public IPlayerWearable TypePlayerWearable { get; set; }

        protected virtual void Awake()
        {
            playerWearable = (T)TypePlayerWearable;
        }
        public virtual void Initialize(GameObject gameObject)
        {
           
        }

    }
}
