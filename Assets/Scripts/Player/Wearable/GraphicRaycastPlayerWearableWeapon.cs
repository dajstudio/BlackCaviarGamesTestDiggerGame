using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackCaviarGame.TestDiggerGame
{
    //TODO: not work after optimization canvasCellManager's prefab
    [CreateAssetMenu(fileName = "GraphicRaycastPlayerWearableWeapon", menuName = "Game/Configuration/PlayerWearable/GraphicRaycastWeapon")]
    public class GraphicRaycastPlayerWearableWeapon : PlayerWearableWeapon
    {

        private GraphicRaycaster _graphicRaycaster;
        private PointerEventData _pointerEventData;
        private EventSystem _eventSystem;

        public override void Initialize()
        {
            base.Initialize();
            _graphicRaycaster = FindObjectOfType<AreaManager>()?.GetComponent<GraphicRaycaster>();
        }
        public override void Shot()
        {
            if (Weapons.Count < 0)
                return;

            if (Input.GetMouseButtonUp(0))
            {
                _pointerEventData = new PointerEventData(_eventSystem);
                _pointerEventData.position = Input.mousePosition;
                List<RaycastResult> raycastResults = new List<RaycastResult>();
                _graphicRaycaster.Raycast(_pointerEventData, raycastResults);

          
                foreach (var result in raycastResults)
                {
                   IInteractable interactable = result.gameObject.GetComponent<IInteractable>();
                    if(interactable != null)
                    {
                        bool isInteracted = interactable.TryInteract();
                        if (isInteracted)
                        {
                            Debug.Log($"Weapon interact with {result.gameObject.name}");
                            Weapons.Pop();
                        }
                        break;
                    }
                }
            }


        }
    }
}