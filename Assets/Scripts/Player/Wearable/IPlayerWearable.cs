using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public interface IPlayerWearable 
    {
        public void Initialize();
        public void Update();
    }
}