using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "CanvasPlayerCreator", menuName = "Game/Creators/PlayerCreator")]
    public class CanvasPlayerCreator : PlayerFromPrefabCreator<CanvasPlayerManager>
    {
        public override void Create(Player player)
        {
           
            base.Create(player);
        }
       
    }
}
