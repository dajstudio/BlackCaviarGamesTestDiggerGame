using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "PlayerNSettings", menuName = "Game/Configuration/Player")]
    public class Player : SerializedScriptableObject
    {
        [AssetsOnly,NonSerialized,OdinSerialize]
       public List<IPlayerWearable> playerWearables;

        public class SaveData
        {
            public List<IPlayerWearable> playerWearables;

            public SaveData(List<IPlayerWearable> playerWearables)
            {
                this.playerWearables = new List<IPlayerWearable>();
                this.playerWearables.AddRange(playerWearables);
            }
        }
        public void Initialize()
        {
            if (playerWearables != null)
            {
                foreach (var playerWearable in playerWearables)
                {
                    playerWearable.Initialize();
                }
            }
        }


    }
}
