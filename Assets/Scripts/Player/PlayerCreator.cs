using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
   
    public abstract class PlayerCreator : SerializedScriptableObject
    {
        public abstract void Create(Player player);

    }
}
