using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "LevelListNSettings", menuName = "Game/Configuration/LevelsList")]
    public class LevelList : SerializedScriptableObject
    {

        public List<LevelStatesList> stateLevels;
        public List<Level> levels;
        public int lastLevelIndex = -1;

        public class LevelStatesList
        {
            public LevelIndex levelIndex;
            public bool isFinished;
            public bool isWin;
        }

        public void ResetStateLevels()
        {
            foreach (var stateLevel in stateLevels)
            {
                stateLevel.isFinished = false;
                stateLevel.isWin = false;
            }
            lastLevelIndex = -1;
        }
        public Level GetLevelOrFirst(int index)
        {
            int levelIndex = levels.FindIndex(x => (x.index.value == index));
            Level level;
            level = GetLevelByListIndexOrFirst(levelIndex);

            return level;
        }

        private Level GetLevelByListIndexOrFirst(int levelIndex)
        {
            Level level;
            if (levelIndex != -1)
            {
                lastLevelIndex = levelIndex;
                level = levels[levelIndex];
            }
            else
            {
                lastLevelIndex = 0;
                level = levels[0];
            }

            return level;
        }

        public Level GetLevelUnpassedOrFirst()
        {
            int levelIndex = stateLevels.FindIndex(x => (x.isFinished == false));
            Level level = GetLevelByListIndexOrFirst(levelIndex);

            return level;
        }
        public Level GetLastLevelOrFirst()
        {
            Level level = GetLevelByListIndexOrFirst(lastLevelIndex != -1 ? lastLevelIndex : 0);

            return level;
        }

    }
}
