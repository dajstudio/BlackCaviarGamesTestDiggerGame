using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
    public class LevelManager : MonoBehaviour, ISavingLoadingItem
    {
        public const string LevelStartEventName = "LevelStartEvent";
        public const string LevelFinishEventName = "LevelFinishEvent";
        public Level currentLevel;
        public LevelList allLevels;


        public string savingLoadingKeyName => name;

        private void OnEnable()
        {
            AddListeners();
        }

        private void AddListeners()
        {
            EventManager.StartListening(GameManager.RestartEventName, ResetLevels);
            EventManager.StartListening(GameManager.NewGameEventName, LoadDefaultLevel);
            EventManager.StartListening(LevelFinishEventName, OnFinishLevel);
            EventManager.StartListening(SaveLoadManager.SaveEventName, Save);
            EventManager.StartListening(SaveLoadManager.LoadEventName, Load);
        }
        [Button]
        public void ResetLevels()
        {
            allLevels.ResetStateLevels();
        }
        [Button]
        public void LoadDefaultLevel()
        {
            ResetLevels();
            StartLevel(allLevels.GetLevelOrFirst(0));
        }

        public void OnFinishLevel()
        {
            //TODO: add UIManager in mainScene with instatiate system on prefabs and link on LevelManager. One of prefabs - FinishLevel You win\lose with restart button
            EventManager.TriggerEvent(GameManager.RestartEventName);
        }
        private void StartLevel(Level level)
        {
            if (currentLevel != null)
            {
                currentLevel.OnAchievedLevelGoal.RemoveListener(FinishLevel);
                Destroy(currentLevel);
            }
            currentLevel = Instantiate(level);
            currentLevel.Initialize();

            currentLevel.OnAchievedLevelGoal.AddListener(FinishLevel);

            EventManager.TriggerEvent(LevelStartEventName);
        }
        private void FinishLevel()
        {
            EventManager.TriggerEvent(LevelFinishEventName);
        }
        [Button]
        public void Load()
        {
           List<LevelList.LevelStatesList> stateLevels = SaveLoadManager.instance.Load<List<LevelList.LevelStatesList>>(savingLoadingKeyName+typeof(List<LevelList.LevelStatesList>).Name);
           if(stateLevels != null)
            {
                allLevels.stateLevels = stateLevels;
            }
            StartLevel(allLevels.GetLastLevelOrFirst());
        }
        [Button]
        public void Save()
        {
            SaveLoadManager.instance.Save<List<LevelList.LevelStatesList>>(savingLoadingKeyName + typeof(List<LevelList.LevelStatesList>).Name, allLevels.stateLevels);
            currentLevel.Save();
        }
     
        private void OnDisable()
        {
            RemoveListeners();
        }

        private void RemoveListeners()
        {
            EventManager.StopListening(GameManager.RestartEventName, ResetLevels);
            EventManager.StopListening(GameManager.NewGameEventName, LoadDefaultLevel);
            EventManager.StopListening(LevelFinishEventName, LoadDefaultLevel);
            EventManager.StopListening(SaveLoadManager.SaveEventName, Save);
            EventManager.StopListening(SaveLoadManager.LoadEventName, Load);
        }
    }
}
