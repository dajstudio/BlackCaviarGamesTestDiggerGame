using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "NLevelIndex", menuName = "Game/Configuration/LevelIndex")]
    public class LevelIndex : ScriptableObject, ISerializeReferenceByAssetGuid
    {
        [ShowInInspector]
        public int value;
    }
}