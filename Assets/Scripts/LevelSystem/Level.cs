using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "LevelNSettings", menuName = "Game/Configuration/Level")]
    public class Level : SerializedScriptableObject, ISavingLoadingItem
    {
        public LevelIndex index;
        [Title("Viewers")]
        // TODO: Extract to LevelCreator
        [Space]
        [LabelText("AreaCreator"),AssetsOnly, ShowInInspector]
        public AreaManagerCreator areaCreator;
        [LabelText("LootsCreator"), AssetsOnly]
        public LootsCreator lootsCreator;
        [LabelText("PlayerCreator"), AssetsOnly, ShowInInspector]
        public PlayerCreator playerCreator;
        // TODO: Extract to LevelGoalChecker
        [Title("LevelGoal")]
        [AssetsOnly, NonSerialized, OdinSerialize]
        public List<LootGroup> levelGoalLoots;
        [AssetsOnly,NonSerialized, OdinSerialize]
        public List<LootGroup> spawningLoots;
        [AssetsOnly, ShowInInspector]
        public LootMachine lootMachine;

        [Title("StartConfigPlayer")]
        [AssetsOnly, NonSerialized, OdinSerialize]
        public List<WeaponGroup> startWeapons;
        [AssetsOnly, ShowInInspector]
        public Bag currentBag;
        public string _currentBagSavingKeyName => savingLoadingKeyName + currentBag.name;

        [Space]
        public Player player;
        [Space, NonSerialized, OdinSerialize]
        public Area area;

        // TODO: change to scene picker;
        [Space]
        public int sceneForCreatingBuildIndex;


        [Title("For generate")]
        [Space, ShowInInspector]
        public AreaGenerator areaGenerator;

        public bool generateEachTime = false;

        public UnityEvent OnAchievedLevelGoal;


        public string savingLoadingKeyName => "levelSettings" +index;

        public bool isLoading = false;
        public SaveLoadData loadData;
        public class SaveLoadData
        {
            public Area area;
            public Dictionary<string, int> spawningLootsSaveData;
            public bool isHaveData;
        }
        [Button]
        public void GenerateNewArea()
        {
           area = areaGenerator?.Generate();
        }

        public void Initialize()
        {
            RemoveListeners();

            if (SaveLoadManager.instance.isHaveData)
            {
                Load();

                if (loadData != null && loadData.isHaveData)
                {
                    area = loadData.area;

                    foreach (var item in loadData.spawningLootsSaveData)
                    {
                        int index = spawningLoots.FindIndex(x => (x.lootType as ScriptableObject).name == item.Key);
                        if (index > -1)
                        {
                            spawningLoots[index].count = item.Value;
                        }
                    }

                    isLoading = true;
                }
            }
            else
            {
                isLoading = false;
            }

            AddAreaAndPlayerToScene();
        }

        private void AddAreaAndPlayerToScene()
        {
            bool isSceneUnloading = false;
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {

                if (SceneManager.GetSceneAt(i).buildIndex == sceneForCreatingBuildIndex)
                {
                    Scene scene = SceneManager.GetSceneAt(i);

                    Debug.Log("Unload");
                    isSceneUnloading = true;
                    UnloadScene(sceneForCreatingBuildIndex);
                }
            }
            if (!isSceneUnloading)
            {
                LoadLevelScene();
            }
        }

        private void UnloadScene(int index)
        {
            AsyncOperation unloadingSceneAsync = SceneManager.UnloadSceneAsync(sceneForCreatingBuildIndex);

            SceneManager.sceneUnloaded += onUnloadLevelScene;
        }
      private void onUnloadLevelScene(Scene scene)
        {
            if(scene.buildIndex == sceneForCreatingBuildIndex)
            {
                Debug.Log($"onUnloadLevelScene sceneIndex - {scene.buildIndex}");
                SceneManager.sceneUnloaded -= onUnloadLevelScene;

                LoadLevelScene();
            }
        }

        private void LoadLevelScene()
        {
            SceneManager.LoadSceneAsync(sceneForCreatingBuildIndex, LoadSceneMode.Additive);

            SceneManager.sceneLoaded += onLoadedLevelScene;
        }

        private void onLoadedLevelScene(Scene scene, LoadSceneMode mode)
        {
            if(scene.buildIndex == sceneForCreatingBuildIndex)
            {
                SceneManager.sceneLoaded -= onLoadedLevelScene;

                SceneManager.SetActiveScene(scene);

                InitializeLootSystem(spawningLoots);
                CreateNewArea(generateEachTime, isLoading);
                InitializeBag();
                CreateNewPlayer();
                AddListeners();
                isLoading = false;
            }
        }
        private void AddListeners()
        {
            Debug.Log($"{this.name} AddListeners");
            currentBag?.OnChangedConsumables.AddListener(CheckLevelGoal);
        }
        private void RemoveListeners()
        {
            currentBag?.OnChangedConsumables.RemoveListener(CheckLevelGoal);
        }
        private void InitializeLootSystem(List<LootGroup> possibleLoot)
        {
            lootMachine.possibleLoot = possibleLoot;
            lootMachine.Initialize();
            lootsCreator.Initialize();
        }

        private void CreateNewPlayer()
        {
            playerCreator.Create(player);
        }

        private void CreateNewArea(bool isGenerateNew, bool isLoading )
        {
            if (isGenerateNew && !isLoading)
            {
                GenerateNewArea();
            }
            areaCreator.Create(
                area
                );
        }
        private void InitializeBag()
        {
            currentBag.Reset();
            if (isLoading)
            {
                currentBag.savingLoadingKeyName = _currentBagSavingKeyName;
                currentBag.Load();
            }
            else
            {
                InitializeWeapon(startWeapons);
            }
        }
        private void InitializeWeapon(List<WeaponGroup> startWeapon)
        {
            
            foreach (var group in startWeapon)
            {
                for (int i = 0; i < group.count; i++)
                {
                    currentBag.AddConsumable((Consumable)group.weapon);
                }
            }
        }
        private void CheckLevelGoal()
        {
            int k = 0;
            foreach (var lootGroup in levelGoalLoots)
            {
               if(lootGroup.count <= currentBag.Consumables.FindAll(x => x as ILooting == lootGroup.lootType).Count)
                {
                    k++;
                }
            }

            if(k >= levelGoalLoots.Count)
            {
                OnAchievedLevelGoal?.Invoke();
                Debug.Log("OnAchiviedLevelGoal.Invoke()");
            }
           
        }
        public void OnDisable()
        {
            RemoveListeners();
        }

        public void Save()
        {

            SaveLoadManager.instance.Save<Area>(savingLoadingKeyName + "Area", area);

            Dictionary<string, int> spawningLootsSaveData = new Dictionary<string, int>();
            for (int i = 0; i < spawningLoots.Count; i++)
            {
                spawningLootsSaveData.Add((spawningLoots[i].lootType as ScriptableObject).name, spawningLoots[i].count);
            }
            SaveLoadManager.instance.Save<Dictionary<string, int>>(savingLoadingKeyName + "spawningLoots", spawningLootsSaveData);

            currentBag.savingLoadingKeyName = _currentBagSavingKeyName;
            currentBag.Save();
        }

        public void Load()
        {
            LoadData();
        }

        private void LoadData()
        {
            loadData = new SaveLoadData();
            loadData.isHaveData = false;
            Area area = new Area();
            area = SaveLoadManager.instance.Load<Area>(savingLoadingKeyName + "Area");
            loadData.area = area;
            Dictionary<string, int> spawningLootsSaveDataCache = SaveLoadManager.instance.Load<Dictionary<string, int>>(savingLoadingKeyName + "spawningLoots");
            loadData.spawningLootsSaveData = spawningLootsSaveDataCache;
            
            if (area != null
                && player != null
                && spawningLootsSaveDataCache != null
                )
            {
                loadData.isHaveData = true;
            }
        }
    }
}
