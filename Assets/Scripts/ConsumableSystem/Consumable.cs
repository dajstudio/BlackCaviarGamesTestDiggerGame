
using Sirenix.OdinInspector;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class Consumable : SerializedScriptableObject,ISerializeReferenceByAssetGuid
    {
        private int _count = 0;
        public int Count
        {
            get { return _count; }
            set { Mathf.Clamp(value, CountMin, CountMax); }
        }
        public int CountMin { get; set; }
        public int CountMax { get; set; }
    }
}