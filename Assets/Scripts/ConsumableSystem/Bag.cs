using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "Bag", menuName = "Game/Configuration/Bag")]
    public class Bag : SerializedScriptableObject, ISavingLoadingItem
    {
        public List<Consumable> Consumables => _consumables;

        public string savingLoadingKeyName { get; set; }

        public UnityEvent OnChangedConsumables;
        [NonSerialized, OdinSerialize]
        private List<Consumable> _consumables;
        public void AddConsumable(Consumable consumable)
        {
            _consumables.Add(consumable);
            OnChangedConsumables?.Invoke();
        }
        public void AddConsumables(List<Consumable> consumables)
        {
            _consumables.AddRange(consumables);
            OnChangedConsumables?.Invoke();
        }
        public void RemoveConsumable(int index)
        {
            if (index < 0)
                return;

            _consumables.RemoveAt(index);

            OnChangedConsumables?.Invoke();
        }
        public void RemoveConsumable(Consumable consumable)
        {
            if (consumable == null)
                return;

            _consumables.Remove(consumable);

            OnChangedConsumables?.Invoke();
        }

        public void Reset()
        {
            _consumables = new List<Consumable>();
            Consumables.AsReadOnly(); 
        }

        public void Save()
        {
            Debug.Log($"{name}.Save()");
            SaveLoadManager.instance.Save<List<Consumable>>(savingLoadingKeyName, _consumables);
        }

        public void Load()
        {
            List<Consumable> consumableCache = SaveLoadManager.instance.Load<List<Consumable>>(savingLoadingKeyName);
           AddConsumables(consumableCache);
        }
    }
}