using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "WeaponSettings", menuName = "Game/Configuration/Weapon")]
    [Serializable]
    public class Weapon : Consumable, IWeapon
    {
      
       
    }
}