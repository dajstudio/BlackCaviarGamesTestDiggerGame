using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class LootsCreator : SerializedScriptableObject
    {
        public const string RequestLootManagerEventName = "RequestLootManager";

        [HideInInspector]
        public UnityEvent<ILooting,Transform> OnPreCreate;
        public abstract void Create(ILooting loot, Transform parent);

        public virtual void Initialize()
        {
            RemoveListeners();
            AddListeners();
        }

        private void AddListeners()
        {
            OnPreCreate.AddListener(Create);
            EventManager.StartListening(RequestLootManagerEventName, ProcessRequestLootViewer);
        }

        public void ProcessRequestLootViewer(GameObject gameObject)
        {
            IHavingLoot havingLoot = gameObject.GetComponent<IHavingLoot>();
            if (havingLoot != null)
            {
                OnPreCreate?.Invoke(havingLoot.loot, gameObject.transform);
            }
          
        }
        private void OnDisable()
        {
            RemoveListeners();

        }

        private void RemoveListeners()
        {
            OnPreCreate.RemoveListener(Create);
            EventManager.StopListening(RequestLootManagerEventName, ProcessRequestLootViewer);
        }
    }
}
