using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    public interface IReceiverLootManager 
    {
        public void AddLootManager(LootManager lootManager);
    }
}
