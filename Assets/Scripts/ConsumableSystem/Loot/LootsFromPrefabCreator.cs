using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections.Generic;

namespace BlackCaviarGame.TestDiggerGame
{
    public abstract class LootsFromPrefabCreator<T> : LootsCreator
                                    where T : LootManager
    {
        [ShowInInspector,NonSerialized,OdinSerialize]
        public List<PrefabLootManagerPair> prefabLootManagerPairs;
        [Serializable]
      public class PrefabLootManagerPair
        {
            public T LootManager;
            [ShowInInspector]
            public ILooting loot;
        }
    }
}
