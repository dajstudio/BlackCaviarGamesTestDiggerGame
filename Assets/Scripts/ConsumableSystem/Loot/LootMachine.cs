using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "LootMachine", menuName = "Game/Creators/LootMachine")]
    public  class LootMachine : SerializedScriptableObject
    {
        public const string RequestGiveMeLootEventName = "GiveMeLootRequest";
        [ShowInInspector]
        public  List<LootGroup> possibleLoot;
        public void Initialize()
        {
            RemoveListeners();
            AddListeners();
        }

        private void AddListeners()
        {
            EventManager.StartListening(RequestGiveMeLootEventName, ProcessRequestGiveMeLoot);
        }

        //TODO: change to delegate
        public void ProcessRequestGiveMeLoot(GameObject gameObject)
        {
            IHavingLoot havingLoot = gameObject.GetComponent<IHavingLoot>();
            if (havingLoot != null)
            {
                havingLoot.ProcessResponseGiveMeLoot(ChooseLoot());
            }
        }
        public  ILooting ChooseLoot()
        {
            
            for (int i = 0; i < possibleLoot.Count; i++)
            {
                if (possibleLoot[i].count > 0)
                {
                    int numberOfPoints = Random.Range(possibleLoot[i].lootType.bound.min, possibleLoot[i].lootType.bound.max);

                    if(possibleLoot[i].lootType.chance > numberOfPoints)
                    {
                        possibleLoot[i].count--;
                        return possibleLoot[i].lootType; ;
                    }
                }
            }
            return null;
        }
        private void OnDisable()
        {
            RemoveListeners();
        }

        private void RemoveListeners()
        {
            EventManager.StopListening(RequestGiveMeLootEventName, ProcessRequestGiveMeLoot);
        }
    } 
}
