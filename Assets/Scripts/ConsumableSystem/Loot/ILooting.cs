
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;

public interface ILooting 
{
    public int chance { get; set; }
    public Bound bound { get; set; } 
    public class Bound
    {
        public int min = 0;
        public int max = 100;

        public Bound(int min, int max)
        {
            this.min = min;
            this.max = max;
        }
    }
}
