using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "NameLootingConsumable", menuName = "Game/Configuration/LootingConsumable")]
    public class LootingConsumable : Consumable, ILooting
    {
        [ShowInInspector, LabelText("Chance (in percent)"),OdinSerialize]
        public int chance { get; set; }
        [ShowInInspector, LabelText("Bound of chance (in percent)")]
        public ILooting.Bound bound { get; set; } = new ILooting.Bound(0, 100);
    }
}