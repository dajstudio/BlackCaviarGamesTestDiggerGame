using UnityEngine.Events;

namespace BlackCaviarGame.TestDiggerGame
{
    public interface IHavingLoot
    {
        public bool isHaveLoot { get; }
        public UnityEvent OnLootAdded { get; set; }
        public UnityEvent OnLootRemoved { get; set; }
        public ILooting loot { get; set; }
        public UnityEvent OnNegativeResponseGiveMeLoot { get; set; }
        public void ProcessResponseGiveMeLoot(ILooting looting);
        public void RequestGiveMeLootData();
        public void AddLoot(ILooting loot);


        public void RemoveLoot();

    }
}
