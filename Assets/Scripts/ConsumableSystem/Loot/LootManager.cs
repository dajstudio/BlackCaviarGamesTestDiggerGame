using Sirenix.OdinInspector;
using UnityEngine;


public abstract class LootManager : MonoBehaviour
{
    [ShowInInspector]
    public ILooting loot;

    public virtual void Initialize(ILooting loot)
    {
        this.loot = loot;
    }

}
