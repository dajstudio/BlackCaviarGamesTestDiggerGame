using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackCaviarGame.TestDiggerGame
{
    [CreateAssetMenu(fileName = "CanvasLootCreator", menuName = "Game/Creators/CanvasLootCreator")]
    public class CanvasLootsCreator : LootsFromPrefabCreator<CanvasLootManager>
    {
        public override void Create(ILooting loot, Transform parent)
        {

            CanvasLootManager canvasLootManager = prefabLootManagerPairs?.Find(x => x.loot == loot)?.LootManager;
            if (canvasLootManager != null)
            {
                LootManager lootManager = Instantiate(canvasLootManager, parent).GetComponent<LootManager>();
                lootManager.Initialize(loot);
            }
        }
    }
}
