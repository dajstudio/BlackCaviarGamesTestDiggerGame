using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackCaviarGame.TestDiggerGame
{
    [RequireComponent(typeof(Image))]
    public class CanvasLootManager : LootManager, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        public Vector3 startPosition;
        [ShowInInspector]
        public IHavingLoot startSpotLootParent;
        [ShowInInspector]
        public IReceiverLootManager finishSpotLootParent;
        private Vector3 _MousePositionInWorld { 
            get => _mousePositionInWorld;
            set => _mousePositionInWorld = new Vector3(value.x, value.y, transform.position.z); 
            }
        private Vector3 _mousePositionInWorld;

        public void OnBeginDrag(PointerEventData eventData)
        {
            startPosition = transform.position;
            startSpotLootParent = transform.parent.GetComponent<IHavingLoot>();
        }
        public void OnDrag(PointerEventData eventData)
        {
            _MousePositionInWorld = Camera.main.ScreenToWorldPoint(new Vector2(eventData.position.x, eventData.position.y));
            transform.position = _MousePositionInWorld;

        }
        public void OnEndDrag(PointerEventData eventData)
        {
            if (finishSpotLootParent != null)
            {
                finishSpotLootParent.AddLootManager(this);
                startSpotLootParent.RemoveLoot();
                
            }
            else
            {
                transform.position = startPosition;
            }
        }

      

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Debug.Log("I'm in");
            IReceiverLootManager receiverLoot = collision.GetComponent<IReceiverLootManager>();
            if(receiverLoot != null)
            {

                finishSpotLootParent = receiverLoot;
            }

        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            IReceiverLootManager receiverLoot = collision.GetComponent<IReceiverLootManager>();
            if (receiverLoot == finishSpotLootParent)
            {
                finishSpotLootParent = null;
            }
        }

    }
}
