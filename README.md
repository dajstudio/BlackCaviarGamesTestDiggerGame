# BlackCaviarGames TestDiggerGame

### Result - https://youtu.be/idcdQlw0Szk

**Digger game**
**Input task:**
- Language: C#
- Development environment: any
- Runtime environment: Unity 2020
- The game is a field of NxN cells (for example, 10x10), where all cells have a certain maximum depth (H, let's say 3).
- The player has M shovels (let's say 20).
- The player can dig in any of the cells by tapping on it. One shovel is spent on each action.
- The cell can be dug several times until the player reaches the maximum depth. After that, it cannot be tapped anymore.
- A "golden nugget" appears in each cell with a certain probability, which must be placed in a special area of the screen (a bag) using drag-and-drop. While the reward is on the cell, it cannot be dug.
- The game ends when the player collects the required amount of rewards (K=3).
- Tasks to be implemented:
- Game mechanics and display of the game field.
- Saving and loading game state (automatically when entering/exiting).
- Restart button.
- Game interface: shovel counter, collected items, area for dragging nuggets (bag).
- Do not implement:
- Beautiful graphics.
- 3D game field.
- Special effects.
- Attention will be paid to:
- Class architecture, interaction between objects, code cleanliness.


**Main goals follow from the input task:**
- Implement gameplay.
- Implement inventory system.
- Implement loot system.
- Implement level system.
- Win based on the amount of collected loot.
- Implement save/load system.
- Build a 2D interface.
- Make it clean.
- Ensure game functionality without errors or data loss.
- Ability to build for Windows platform.


**Additional goals:**
- In the development conditions, outside the framework of a specific product and without setting deadlines, some experimentation is allowed.
- Generate the game field from pre-set presets.
- Output convenient settings for game elements through the engine's graphical interface.
- Create the ability to assemble cell functionality from elements, assuming independence of cells from each other on the field.
- Create the ability to replace the display of the game field - Canvas (chosen for example due to quick assembly on the scene), Sprite renderer, 3D renderer.
- Separate information for saving into a separate segment.
- Include excessive scalability in the architecture, for example, the ability to add three fields.
- Create a level management system.
- Minimize save points.
- Do not use the ECS framework.
- Create a diagram.


**Remaining tasks:**
- Break down the level into generation, creation, and state control.
- Output the assembly of cell functionality from elements into the engine's graphical interface.
- Minor fixes (see TODO in the code).


**Time spent:** 46 hours.
